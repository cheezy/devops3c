This is a project for a workshop.

The build the application you should execute:

    ./gradlew clean build
    
To run the application you should execute:

    ./gradlew bootRun
    
To run the unit tests you should execute:

    ./gradlew test


To runt he cucumber tests you should execute:

    ./gradlew integrationTest 
